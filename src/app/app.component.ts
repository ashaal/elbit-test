import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'elbit-test';
  btnTitle = 'הסתר';

  isShown = true;

  onClick() {
    this.isShown = !this.isShown;
    this.btnTitle = this.isShown ? 'הסתר' : 'הראה';
  }
}
